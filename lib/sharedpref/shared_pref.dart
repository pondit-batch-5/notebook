import 'package:shared_preferences/shared_preferences.dart';

class PrefManagement {
  static SharedPreferences? pref;
  static const String userId = 'user-id';
  static const String isLoggedIn = 'is-logged-in';

  static Future<void> init() async {
    if (pref == null) {
      pref = await SharedPreferences.getInstance();
    }
  }

  static void setIsLoggedIn(bool value) {
    pref!.setBool(isLoggedIn, value);
  }

  static bool? getIsLoggedIn() {
    return pref!.getBool(isLoggedIn);
  }

  static void setUserId(int value) {
    pref!.setInt(userId, value);
  }

  static int? getUserId() {
    return pref!.getInt(userId);
  }

  static void clearPref() {
    pref!.clear();
  }
}
