import 'package:flutter/material.dart';
import 'package:notebook/pages/login_page.dart';
import 'package:notebook/sharedpref/shared_pref.dart';
import 'package:notebook/viewmodel/user_viewmodel.dart';
import 'package:provider/provider.dart';

class DrawerPage extends StatelessWidget {
  const DrawerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<UserViewModel>(builder: (_, userViewModel, ___) {
      return Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(userViewModel.userData.name ?? ''),
                  Text(
                    userViewModel.userData.email ?? '',
                    style: TextStyle(
                      fontSize: 13,
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              title: Text('Logout'),
              onTap: () {
                PrefManagement.clearPref();
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
            ),
          ],
        ),
      );
    });
  }
}
