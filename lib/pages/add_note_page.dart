import 'package:flutter/material.dart';
import 'package:notebook/viewmodel/notebook_viewmodel.dart';
import 'package:provider/provider.dart';

class NoteAddPage extends StatefulWidget {
  const NoteAddPage({Key? key}) : super(key: key);

  @override
  State<NoteAddPage> createState() => _NoteAddPageState();
}

class _NoteAddPageState extends State<NoteAddPage> {
  @override
  void initState() {
    super.initState();
    NotebookViewModel notebookViewModel =
    Provider.of<NotebookViewModel>(context, listen: false);
    notebookViewModel.resetAddPage();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NotebookViewModel>(builder: (_, provider, ___) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Add Note',
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 16,
                ),
                child: Text(
                  'Note Title *',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 32, right: 32),
                child: TextField(
                  controller: provider.titleTextEditingController,
                  decoration: InputDecoration(
                    hintText: '',
                    fillColor: Colors.grey.shade400,
                    filled: true,
                    border: InputBorder.none,
                  ),
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 16,
                ),
                child: Text(
                  'Note Content *',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 32, right: 32),
                child: TextField(
                  controller: provider.contentTextEditingController,
                  decoration: InputDecoration(
                    hintText: '',
                    fillColor: Colors.grey.shade400,
                    filled: true,
                    border: InputBorder.none,
                  ),
                  style: const TextStyle(
                    fontSize: 15,
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 8.0,
                  vertical: 16,
                ),
                child: Text(
                  'Note Date *',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                color: Colors.grey.shade400,
                margin: const EdgeInsets.only(left: 32, right: 32),
                padding: const EdgeInsets.only(left: 8, right: 8),
                child: ListTile(
                  contentPadding: const EdgeInsets.all(0),
                  title: Text(provider.selectedDate == ''
                      ? 'Select date'
                      : provider.selectedDate),
                  onTap: () {
                    showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2050),
                    ).then((value) {
                      provider.selectedDate = value.toString().substring(0, 10);
                    });
                  },
                  trailing: const Icon(Icons.calendar_month_outlined),
                ),
              ),
              const SizedBox(
                height: 32,
              ),
              Center(
                child: InkWell(
                  onTap: () {
                    provider.insertNoteVM(context);
                  },
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 25, top: 25),
                    padding: const EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 10,
                      bottom: 10,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.blue,
                    ),
                    child: const Text(
                      'Save Note',
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
