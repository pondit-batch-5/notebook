import 'package:flutter/material.dart';
import 'package:notebook/database/database_management.dart';
import 'package:notebook/pages/add_note_page.dart';
import 'package:notebook/pages/drawer_page.dart';
import 'package:notebook/pages/update_note_page.dart';
import 'package:notebook/util/util.dart';
import 'package:notebook/viewmodel/notebook_viewmodel.dart';
import 'package:notebook/viewmodel/user_viewmodel.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DatabaseManager db = DatabaseManager();

  @override
  void initState() {
    super.initState();
    NotebookViewModel notebookViewModel =
        Provider.of<NotebookViewModel>(context, listen: false);
    notebookViewModel.fetchNoteListVM();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<NotebookViewModel>(
        builder: (_, notebookViewModelProvider, __) {
      return Consumer<UserViewModel>(
        builder: (_, userViewModel,___) {
          return RefreshIndicator(
            onRefresh: () async {},
            child: notebookViewModelProvider.isLoading == true
                ? const Scaffold(
                    body: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Scaffold(
                    appBar: AppBar(
                      title: const Text('Note Book'),
                    ),
                    drawer: DrawerPage(),
                    floatingActionButton: Container(
                      margin: const EdgeInsets.only(right: 20, bottom: 20),
                      child: FloatingActionButton(
                        onPressed: () async {
                          bool? isAdded = await Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const NoteAddPage(),
                            ),
                          );
                          if (isAdded != null && isAdded == true) {
                            notebookViewModelProvider.fetchNoteListVM();
                          }
                        },
                        child: const Icon(Icons.add),
                      ),
                    ),
                    body: Padding(
                      padding:
                          const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(Icons.menu_book),
                              SizedBox(
                                width: 4,
                              ),
                              Text('Hello, ${userViewModel.userData.name}'),
                              SizedBox(
                                width: 2,
                              ),
                              Text(Util.greeting()),
                            ],
                          ),
                          TextField(
                            decoration: InputDecoration(
                              labelText: 'Search by title',
                              filled: true,
                              fillColor: Colors.grey.shade400,
                              border: InputBorder.none,
                              suffixIcon: const Icon(Icons.search),
                            ),
                            onChanged: (query){
                              notebookViewModelProvider.filterByQuery(query);
                            },
                          ),
                          /* notebookViewModelProvid
                          r.isLoading == false
                              ? CircularProgressIndicator()
                              : */

                          SizedBox(height: 16,),
                          Expanded(
                            child: ListView.separated(
                              separatorBuilder: (contex, index) {
                                return const SizedBox(
                                  height: 8,
                                );
                              },
                              itemCount: notebookViewModelProvider.noteList.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  padding: const EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.blueAccent,
                                    ),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            notebookViewModelProvider
                                                    .noteList[index].title ??
                                                '',
                                            style: const TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text(
                                            notebookViewModelProvider
                                                    .noteList[index].content ??
                                                '',
                                            style: const TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                          Text(
                                            notebookViewModelProvider
                                                    .noteList[index].date ??
                                                '',
                                            style: const TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ],
                                      ),
                                      PopupMenuButton(
                                        icon: const Icon(Icons.more_vert),
                                        itemBuilder: (context) {
                                          return [
                                            const PopupMenuItem(
                                              value: 0,
                                              child: Row(
                                                children: [
                                                  Icon(Icons.edit),
                                                  SizedBox(
                                                    width: 8,
                                                  ),
                                                  Text('Update'),
                                                ],
                                              ),
                                            ),
                                            const PopupMenuItem(
                                              value: 1,
                                              child: Row(
                                                children: [
                                                  Icon(Icons.delete),
                                                  SizedBox(
                                                    width: 8,
                                                  ),
                                                  Text('Delete'),
                                                ],
                                              ),
                                            ),
                                          ];
                                        },
                                        onSelected: (value) async {
                                          if (value == 0) {
                                            bool? isUpadted = await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) => NoteUpdatePage(
                                                    notebook:
                                                        notebookViewModelProvider
                                                            .noteList[index]),
                                              ),
                                            );
                                            if (isUpadted != null &&
                                                isUpadted == true) {
                                              notebookViewModelProvider
                                                  .fetchNoteListVM();
                                            }
                                          } else if (value == 1) {
                                            notebookViewModelProvider.deleteNoteVM(
                                                notebookViewModelProvider
                                                    .noteList[index].id!,
                                                context);
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
          );
        }
      );
    });
  }
}
