import 'package:flutter/material.dart';
import 'package:notebook/viewmodel/user_viewmodel.dart';
import 'package:provider/provider.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({super.key});

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  @override
  void initState() {
    super.initState();
    UserViewModel userViewModel =
        Provider.of<UserViewModel>(context, listen: false);
    userViewModel.resetRegisterPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<UserViewModel>(builder: (_, userViewModel, ___) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: userViewModel.nameTxtCtl,
                decoration: InputDecoration(
                  hintText: 'Enter Your Name',
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  border: InputBorder.none,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              TextField(
                controller: userViewModel.emailTxtCtl,
                decoration: InputDecoration(
                  hintText: 'Enter Your Email',
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  border: InputBorder.none,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              TextField(
                obscureText: true,
                controller: userViewModel.passwordTxtCtl,
                decoration: InputDecoration(
                  hintText: 'Enter Your Password',
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  border: InputBorder.none,
                ),
              ),
              InkWell(
                onTap: () {
                  userViewModel.validateRegistrationForm(context);
                },
                child: Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                  child: Text(
                    'REGISTER',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(),
                  child: Text(
                    'Already registered? Login',
                    style: TextStyle(
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
