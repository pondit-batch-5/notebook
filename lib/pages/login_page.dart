import 'package:flutter/material.dart';
import 'package:notebook/pages/registration_page.dart';
import 'package:notebook/viewmodel/user_viewmodel.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Consumer<UserViewModel>(builder: (_, userViewModel, ___) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: userViewModel.emailTxtCtl,
                decoration: InputDecoration(
                  hintText: 'Enter Your Email',
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  border: InputBorder.none,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              TextField(
                obscureText: true,
                controller: userViewModel.passwordTxtCtl,
                decoration: InputDecoration(
                  hintText: 'Enter Your Password',
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  border: InputBorder.none,
                ),
              ),
              InkWell(
                onTap: () {
                  userViewModel.validateLoginnForm(context);
                },
                child: Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                  child: Text(
                    'LOGIN',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RegistrationPage()));
                },
                child: Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(),
                  child: Text(
                    'New to App? Resigter',
                    style: TextStyle(
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
