import 'package:flutter/material.dart';

void main() {
  runApp(Page1());
}

class Page1 extends StatelessWidget {
  const Page1({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Page2(
        myValue: 'Value from page 1',
      ),
    );
  }
}

class Page2 extends StatelessWidget {
  final String? myValue;

  const Page2({super.key, this.myValue});

  @override
  Widget build(BuildContext context) {
    return Page3(
      myValue: myValue,
    );
  }
}

class Page3 extends StatelessWidget {
  final String? myValue;

  const Page3({super.key, this.myValue});

  @override
  Widget build(BuildContext context) {
    return Page4(
      myValue: myValue,
    );
  }
}

class Page4 extends StatelessWidget {
  final String? myValue;

  const Page4({super.key, this.myValue});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('$myValue')),
    );
  }
}
