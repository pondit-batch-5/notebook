import 'dart:async';

import 'package:flutter/material.dart';
import 'package:notebook/pages/home_page.dart';
import 'package:notebook/pages/login_page.dart';
import 'package:notebook/sharedpref/shared_pref.dart';
import 'package:notebook/viewmodel/user_viewmodel.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({super.key});

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    initSharePref();
  }

  initSharePref() async {
    await PrefManagement.init();
    redirectToMyPage();
  }

  void redirectToMyPage() {
    Timer(
      const Duration(seconds: 2),
      () {
        if (PrefManagement.getIsLoggedIn() != null &&
            PrefManagement.getIsLoggedIn() == true) {

          UserViewModel userViewModel =  Provider.of<UserViewModel>(context,listen: false);
          userViewModel.getUserById(context);
        } else {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginPage()));
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        color: Colors.blue,
        child: const Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Notebook',
              style: TextStyle(
                fontSize: 38,
                color: Colors.white,
              ),
            ),
            SizedBox(
              width: 200,
              height: 3,
              child: LinearProgressIndicator(),
            ),
          ],
        ),
      ),
    );
  }
}
