import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:notebook/database/database_management.dart';
import 'package:notebook/models/notebook.dart';
import 'package:notebook/sharedpref/shared_pref.dart';

class NotebookViewModel extends ChangeNotifier {
  DatabaseManager db = DatabaseManager();
  bool? _isLoading = false;
  TextEditingController titleTextEditingController = TextEditingController();
  TextEditingController contentTextEditingController = TextEditingController();
  String _selectedDate = '';
  List<Notebook> noteList = [];
  List<Notebook> noteListContainer = [];

  String get selectedDate => _selectedDate;

  set selectedDate(String value) {
    _selectedDate = value;
    notifyListeners();
  }

  bool? get isLoading => _isLoading;

  set isLoading(bool? value) {
    _isLoading = value;
  }

  void resetNotebook() {
    isLoading = true;
    noteList = [];
    noteListContainer = [];
    notifyListeners();
  }

  void notifier() {
    notifyListeners();
  }

  void resetAddPage() {
    _selectedDate = '';
    titleTextEditingController.clear();
    contentTextEditingController.clear();
  }

  void resetUpdatePage(Notebook notebook) {
    _selectedDate = notebook.date ?? '';
    titleTextEditingController.text = notebook.title ?? '';
    contentTextEditingController.text = notebook.content ?? '';
  }

  void fetchNoteListVM() async {
    resetNotebook();
    try {
      noteList = await db.fetchNoteList(PrefManagement.getUserId()!);
      noteListContainer = noteList;
    } catch (error) {
      print(error.toString());
    } finally {
      isLoading = false;
      notifyListeners();
    }
  }

  void insertNoteVM(BuildContext mContext) async {
    if (titleTextEditingController.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Please enter title'),
        ),
      );
    } else if (contentTextEditingController.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Please enter content'),
        ),
      );
    } else if (selectedDate == '') {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Please select date'),
        ),
      );
    } else {
      Notebook notebook = Notebook(
        title: titleTextEditingController.text,
        content: contentTextEditingController.text,
        date: selectedDate,
        userid: PrefManagement.getUserId(),
      );
      int insert = await db.insertNote(notebook);
      if (insert > 0) {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Note has been successfuly added'),
          ),
        );
        Navigator.pop(mContext, true);
      } else {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Note can not be added right now'),
          ),
        );
      }

      print(insert);
    }
  }

  void updateNoteVm(int noteId, BuildContext mContext) async {
    if (titleTextEditingController.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Please enter title'),
        ),
      );
    } else if (contentTextEditingController.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Please enter content'),
        ),
      );
    } else if (selectedDate == '') {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Please select date'),
        ),
      );
    } else {
      Notebook notebook = Notebook(
        id: noteId,
        title: titleTextEditingController.text,
        content: contentTextEditingController.text,
        date: selectedDate,
        userid: PrefManagement.getUserId(),
      );
      int update = await db.updateNote(notebook);
      if (update > 0) {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Note has been successfuly updated'),
          ),
        );
        Navigator.pop(mContext, true);
      } else {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Note can not be updated right now'),
          ),
        );
      }
    }
  }

  void deleteNoteVM(int id, BuildContext mContext) async {
    try {
      int isAdded = await db.deleteNote(id);
      if (isAdded > 0) {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Note deleted'),
          ),
        );
        fetchNoteListVM();
      } else {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Note can not be deleted'),
          ),
        );
      }
    } catch (error) {
      print(error.toString());
    }
  }

  void filterByQuery(String query) {
    if (query.isEmpty) {
      noteList = noteListContainer;
    } else {
      List<Notebook> mList = [];
      for (Notebook notebook in noteListContainer) {
        if (notebook.title!.toLowerCase().contains(query.toLowerCase()) ||
            notebook.content!.toLowerCase().contains(query.toLowerCase())) {
          mList.add(notebook);
        }
      }

      noteList = mList;
    }

    notifyListeners();
  }
}
