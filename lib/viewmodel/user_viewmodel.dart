import 'package:flutter/material.dart';
import 'package:notebook/models/user_model.dart';
import 'package:notebook/pages/home_page.dart';
import 'package:notebook/repository/user_repository.dart';
import 'package:notebook/sharedpref/shared_pref.dart';

class UserViewModel extends ChangeNotifier {
  final UserRepository userRepository = UserRepository();
  TextEditingController emailTxtCtl = TextEditingController();
  TextEditingController nameTxtCtl = TextEditingController();
  TextEditingController passwordTxtCtl = TextEditingController();
  UserModel userData = UserModel();
  bool? isLoading;

  void validateRegistrationForm(BuildContext mContext) async {
    if (nameTxtCtl.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Name is required'),
        ),
      );
    } else if (emailTxtCtl.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Email is required'),
        ),
      );
    } else if (passwordTxtCtl.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Password is required'),
        ),
      );
    } else if (passwordTxtCtl.text.length < 6) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Password is must be at least 6 character'),
        ),
      );
    } else {
      bool isExist = await userRepository.isUserExistRepo(emailTxtCtl.text);
      if (isExist == true) {
        ScaffoldMessenger.of(mContext).showSnackBar(
          SnackBar(
            content: Text('${emailTxtCtl.text} already exist'),
          ),
        );
      } else {
        UserModel userModel = UserModel(
            name: nameTxtCtl.text,
            email: emailTxtCtl.text,
            password: passwordTxtCtl.text);
        int isRegister = await userRepository.registerUserRepo(userModel);
        if (isRegister > 0) {
          ScaffoldMessenger.of(mContext).showSnackBar(
            SnackBar(
              content: Text('${emailTxtCtl.text} successfully registered'),
            ),
          );
          Navigator.pop(mContext);
        }
      }
    }
  }

  void validateLoginnForm(BuildContext mContext) async {
    if (emailTxtCtl.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Email is required'),
        ),
      );
    } else if (passwordTxtCtl.text.isEmpty) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Password is required'),
        ),
      );
    } else if (passwordTxtCtl.text.length < 6) {
      ScaffoldMessenger.of(mContext).showSnackBar(
        const SnackBar(
          content: Text('Password is must be at least 6 character'),
        ),
      );
    } else {
      UserModel userModel =
          await userRepository.fetchUserByEmailAndPasswordRepo(
              emailTxtCtl.text, passwordTxtCtl.text);

      if (userModel.id != null) {
        userData = userModel;
        PrefManagement.setIsLoggedIn(true);
        PrefManagement.setUserId(userData.id!);
        Navigator.push(
            mContext, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        ScaffoldMessenger.of(mContext).showSnackBar(
          const SnackBar(
            content: Text('Credential mismatch'),
          ),
        );
      }
    }
  }

  void getUserById(BuildContext mContext) async {
    UserModel userModel =
        await userRepository.fetchUserByIdRepo(PrefManagement.getUserId() ?? 0);
    if (userModel.id != null) {
      userData = userModel;
      Navigator.push(
          mContext, MaterialPageRoute(builder: (context) => HomePage()));
    }
  }

  void resetRegisterPage() {
    emailTxtCtl.clear();
    nameTxtCtl.clear();
    passwordTxtCtl.clear();
  }
}
