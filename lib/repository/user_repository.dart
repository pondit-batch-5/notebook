import 'package:notebook/database/database_management.dart';
import 'package:notebook/models/user_model.dart';

class UserRepository {
  final DatabaseManager db = DatabaseManager();

  Future<UserModel> fetchUserByEmailAndPasswordRepo(
      String email, String password) async {
    return await db.fetchUserByEmailAndPassword(email, password);
  }

  Future<UserModel> fetchUserByIdRepo(int id) async {
    return await db.fetchUserById(id);
  }

  Future<bool> isUserExistRepo(String email) async {
    return await db.isUserExist(email);
  }

  Future<int> registerUserRepo(UserModel userModel) async {
    return await db.registerUser(userModel);
  }
}
